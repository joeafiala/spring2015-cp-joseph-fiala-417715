Fiala.MainMenu = function(game) {
};
Fiala.MainMenu.prototype = {
    create: function(){
        this.add.sprite(0, 0, 'mainmenu');
        
        
        //adds the "start" button; when clicked performs the startGame function
        this.add.button(290, 250, 'startText', this.startGame, this);
        
    },
    startGame: function(){
        //if the level is set to 0, begins the first level; otherwise begins the second level
        if(Fiala.picklevel == 0) this.state.start('Game');
        else if(Fiala.picklevel == 1) this.state.start('Level');
    },
    //used for testing purposes
    nextLevel: function(){
        this.state.start('Level');
    }
};