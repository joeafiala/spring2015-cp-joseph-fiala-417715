Fiala.Preloader = function(game){
    Fiala.GAME_WIDTH = 800;
    Fiala.GAME_HEIGHT = 600;
};
Fiala.Preloader.prototype = {
    preload: function(){
        //loads the images used in the game
        this.load.image('sky', 'assets/sky.png');
        this.load.image('ground', 'assets/platform.png');
        this.load.image('star', 'assets/star.png');
        this.load.image('startText', 'assets/startText.png');
        this.load.image('trophy', 'assets/trophy.png');
        this.load.image('diamond', 'assets/diamond.png');
        this.load.image('cave', 'assets/cave.png');
        this.load.image('bricks', 'assets/bricks.png');
        this.load.image('caveground', 'assets/caveground.png');
        this.load.image('mainmenu', 'assets/mainmenu.png');
        this.load.spritesheet('coin', 'assets/coin.png', 32, 32);
        this.load.spritesheet('dude', 'assets/dude.png', 32, 48);
        this.load.spritesheet('baddie', 'assets/baddie.png', 32, 32);
        this.load.spritesheet('bird', 'assets/bird.png', 32, 53);
        this.load.spritesheet('lava', 'assets/lavas.png', 324, 40);
        this.load.spritesheet('water', 'assets/water.png', 323, 40);
        this.load.spritesheet('fireball', 'assets/fireball.png', 10, 13);
        

    },
    create: function(){
        this.state.start('MainMenu');
    }
};