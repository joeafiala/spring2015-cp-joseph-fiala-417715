Fiala.Levels = function(game){
    //sets variables to null to start
    this.player = null;
    this.platforms = null;
    this.ground = null;
    this.buttonFun = null;
    this.reward = null;
    this.diamond = null;
    this.coin = null;
    this.coin2 = null;
    this.lava = null;
    this.lava2 = null;
    this.lava3 = null;
    
    this.fireball1 = null;
    this.fireball2 = null;
    this.fireball3 = null;
    this.fireball4 = null;
};
Fiala.Levels.prototype = {
    preload: function(){
        //loads background image
        for(var i = 0; i < 3; i++){
            var newX = i * 800;
            this.add.sprite(newX, 0, 'cave');
        }
    },
    create: function(){
        //sets world to size 2000x600
        this.game.world.setBounds(0, 0, 2000, 600);
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        //adds platform group
        this.platforms = this.add.group();
        this.platforms.enableBody = true;
        
        //sets ground for the current level
        this.ground = this.platforms.create(0, this.game.world.height - 64, 'caveground');
        this.ground.scale.setTo(10, 2);
        this.ground.body.immovable = true;
        
        //below I create the fireballs "enemy" and add their animations
        //they are all the same, just with different names (fireball1, fireball2, ...)
        this.fireball1 = this.game.add.sprite(375, 550, 'fireball');
        this.fireball1.scale.setTo(2, 2);
        this.game.physics.arcade.enable(this.fireball1);
        
        this.fireball1.animations.add('up', [0, 1, 2, 3, 4], 5, true);
        this.fireball1.animations.add('down', [5, 6, 7, 8, 9], 5, true);
        
        this.fireball2 = this.game.add.sprite(800, 550, 'fireball');
        this.fireball2.scale.setTo(2, 2);
        this.game.physics.arcade.enable(this.fireball2);
        
        this.fireball2.animations.add('up', [0, 1, 2, 3, 4], 5, true);
        this.fireball2.animations.add('down', [5, 6, 7, 8, 9], 5, true);
        
        this.fireball3 = this.game.add.sprite(1150, 550, 'fireball');
        this.fireball3.scale.setTo(2, 2);
        this.game.physics.arcade.enable(this.fireball3);
        
        this.fireball3.animations.add('up', [0, 1, 2, 3, 4], 5, true);
        this.fireball3.animations.add('down', [5, 6, 7, 8, 9], 5, true);
        
        this.fireball4 = this.game.add.sprite(1700, 550, 'fireball');
        this.fireball4.scale.setTo(2, 2);
        this.game.physics.arcade.enable(this.fireball4);
        
        this.fireball4.animations.add('up', [0, 1, 2, 3, 4], 5, true);
        this.fireball4.animations.add('down', [5, 6, 7, 8, 9], 5, true);
        
        //below I create the lava ground element that is similar to the water element from the last level
        //this will kill the player if the player lands on it
        this.lava = this.game.add.sprite(300, 523, 'lava');
        this.game.physics.arcade.enable(this.lava);
        this.lava.scale.setTo(2, 1);
        
        this.lava2 = this.game.add.sprite(1050, 523, 'lava');
        this.game.physics.arcade.enable(this.lava2);
        this.lava2.scale.setTo(2, 1);
        this.lava3 = this.game.add.sprite(1635, 523, 'lava');
        this.game.physics.arcade.enable(this.lava3);
        this.lava3.scale.setTo(2, 1);
        
        //creates animation for each of the lavas
        this.lava.animations.add('movingLava', [1, 2, 3, 4, 5, 6, 7, 4], 5, true);
        this.lava2.animations.add('movingLava', [1, 2, 3, 4, 5, 6, 7, 4], 5, true);
        this.lava3.animations.add('movingLava', [1, 2, 3, 4, 5, 6, 7, 4], 5, true);
        
        //creates the ledges for the level
        this.ground = this.platforms.create(500, 350, 'caveground');
        this.ground.scale.setTo(0.5, 1);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(1300, 450, 'caveground');
        this.ground.scale.setTo(0.25, 1);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(1500, 250, 'caveground');
        this.ground.scale.setTo(0.25, 1);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(1300, 50, 'caveground');
        this.ground.scale.setTo(0.25, 1);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(1800, 50, 'caveground');
        this.ground.scale.setTo(0.25, 1);
        this.ground.body.immovable = true;
        
        //creates the finish line/trophy
        this.trophy = this.add.sprite(1855, 0, 'trophy');
        this.game.physics.arcade.enable(this.trophy);
        this.trophy.body.gravity.y = 300;
        
        //creates reward group (not used in current implementation)
        this.reward = this.add.group();
        this.reward.enableBody = true;
        
        //adds diamond reward
        this.diamond = this.game.add.sprite(1320, 0, 'diamond');
        this.game.physics.arcade.enable(this.diamond);
        this.diamond.body.gravity.y = 300;
        
        //adds coin reward
        this.coin = this.reward.create(350, 100, 'coin');
        this.coin2 = this.reward.create(950, 100, 'coin');
        //adds coin animation (rotates the coin)
        this.coin.animations.add('rotate', [0, 1, 2, 3, 4, 5], 10, true);
        this.coin2.animations.add('rotate', [0, 1, 2, 3, 4, 5], 10, true);
        
        //adds player to game and enables physics on player
        this.player = this.game.add.sprite(32, 32, 'dude');
        this.game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 300;
        this.player.body.bounce.y = 0.2;
        this.player.body.collideWorldBounds = true;
        
        //sets animations for the player movement
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);
        
        //sets movement button for player based on keyboard keys
        this.buttonFun = this.game.input.keyboard.createCursorKeys();
        
        //follows the player when too close to edge
        this.camera.follow(this.player);
    },
    update: function(){

        //sets collisions in game
        this.game.physics.arcade.collide(this.player, this.platforms);
        this.game.physics.arcade.collide(this.trophy, this.platforms);
        this.game.physics.arcade.collide(this.diamond, this.platforms);
        
        /////////////
        //sets fireball movements
        if(this.fireball1.body.y > 535){
            this.fireball1.body.velocity.y = -75;
        }
        if(this.fireball1.body.y < 275){
            this.fireball1.body.velocity.y = 75;
        }
        if(this.fireball1.body.velocity.y < 0){
            this.fireball1.animations.play('up');
        }
        else{
            this.fireball1.animations.play('down');
        }

        if(this.fireball2.body.y > 535){
            this.fireball2.body.velocity.y = -75;
        }
        if(this.fireball2.body.y < 150){
            this.fireball2.body.velocity.y = 75;
        }
        if(this.fireball2.body.velocity.y < 0){
            this.fireball2.animations.play('up');
        }
        else{
            this.fireball2.animations.play('down');
        }
        
        if(this.fireball3.body.y > 535){
            this.fireball3.body.velocity.y = -75;
        }
        if(this.fireball3.body.y < 100){
            this.fireball3.body.velocity.y = 75;
        }
        if(this.fireball3.body.velocity.y < 0){
            this.fireball3.animations.play('up');
        }
        else{
            this.fireball3.animations.play('down');
        }
        
        if(this.fireball4.body.y > 535){
            this.fireball4.body.velocity.y = -75;
        }
        if(this.fireball4.body.y < 50){
            this.fireball4.body.velocity.y = 75;
        }
        if(this.fireball4.body.velocity.y < 0){
            this.fireball4.animations.play('up');
        }
        else{
            this.fireball4.animations.play('down');
        }
        //////////////////
        

        //sets player velocity to 0 so the player stops when not pressing a keyboard arrow key
        this.player.body.velocity.x = 0;
        
        //stars animation of coins/lava
        this.coin.animations.play('rotate');
        this.coin2.animations.play('rotate');
        this.lava.animations.play('movingLava');
        this.lava2.animations.play('movingLava');
        this.lava3.animations.play('movingLava');
        
        //overlap between player and any coin
        //removes coin and adds to player score
        this.game.physics.arcade.overlap(this.player, this.reward, function(player, coin){
            coin.kill();
            Fiala.score += 25;
        }, null, this.game);
        
        //overlap between player and diamond
        //removes diamond and adds to player score
        this.game.physics.arcade.overlap(this.player, this.diamond, function(player, diamond){
            diamond.kill();
            Fiala.score += 50;
        }, null, this.game);
        
        //overlap between player and lava
        //kills player
        //sets score to 0
        //moves player back to first level
        //displays "GAME OVER" message and score
        this.game.physics.arcade.overlap(this.player, this.lava, function(player, lava){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        this.game.physics.arcade.overlap(this.player, this.lava2, function(player, lava){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        this.game.physics.arcade.overlap(this.player, this.lava3, function(player, lava){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //overlap between player and fireball
        //kills player
        //sets score to 0
        //moves player back to first level
        //displays "GAME OVER" message and score
        this.game.physics.arcade.overlap(this.player, this.fireball1, function(player, fireball){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        this.game.physics.arcade.overlap(this.player, this.fireball2, function(player, fireball){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        this.game.physics.arcade.overlap(this.player, this.fireball3, function(player, fireball){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        this.game.physics.arcade.overlap(this.player, this.fireball4, function(player, fireball){
            player.kill();
            Fiala.picklevel = 0;
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //overlap between player and trophy
        //removes trophy
        //displays "VICTORY" message and score
        //resets score after delay and moves back to main menu
        //makes starting level the first level again
        this.game.physics.arcade.overlap(this.player, this.trophy, function(player, trophy){
            trophy.kill();
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'VICTORY! \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            Fiala.picklevel = 0;
            this.time.events.add(4000, function(){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //sets movement for player based on keyboard arrow key
        if(this.buttonFun.left.isDown){
            this.player.body.velocity.x = -150;
            this.player.animations.play('left');
        }
        else if(this.buttonFun.right.isDown){
            this.player.body.velocity.x = 150;
            this.player.animations.play('right');
        }
        else{
            this.player.animations.stop();
            this.player.frame = 4;
        }
        
        if(this.buttonFun.up.isDown && this.player.body.touching.down){
            this.player.body.velocity.y = -350;
        }
        
    },
    render: function(){
        //displays player score
        this.game.debug.text('Score: ' + Fiala.score, 32, 32);

    }
};