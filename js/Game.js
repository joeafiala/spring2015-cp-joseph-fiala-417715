Fiala.Game = function(game){
    //sets the variables to null to start
    this.player = null;
    this.platforms = null;
    this.buttonFun = null;
    this.enemy1 = null;
    this.enemy2 = null;
    this.bird = null;
    this.pause = null;
    this.trophy = null;
    this.diamond = null;
    
    this.stars = null;
    this.scoreText = null;
    
    this.ground = null;
    this.ledge = null;
    this.water = null;
};
Fiala.Game.prototype = {
    preload: function(){
        //adds the background to the game
        for(var i = 0; i < 3; i++){
            var newX = i * 800;
            this.add.sprite(newX, 0, 'sky');
        }
    },
    create: function(){
        //sets gameworld to be of size 2000x600
        this.game.world.setBounds(0, 0, 2000, 600);
        this.physics.startSystem(Phaser.Physics.ARCADE);
        
        //creates a group of platforms
        this.platforms = this.add.group();
        this.platforms.enableBody = true;
        
        //adds the end goal/finish line
        this.trophy = this.add.sprite(1955, 300, 'trophy');
        this.game.physics.arcade.enable(this.trophy);
        this.trophy.body.gravity.y = 300;
        
        //sets the ground
        this.ground = this.platforms.create(0, this.game.world.height - 64, 'ground');
        this.ground.scale.setTo(10, 2);
        this.ground.body.immovable = true;
        
        //the following sets the platforms and further ground elements
        this.ground = this.platforms.create(1950, 350, 'ground');
        this.ground.scale.setTo(0.2, 8);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(1250, 350, 'bricks');
        this.ground.scale.setTo(0.2, 1);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(1600, 250, 'bricks');
        this.ground.scale.setTo(0.6, 1);
        this.ground.body.immovable = true;
        
        this.ground = this.platforms.create(900, 230, 'bricks');
        this.ground.scale.setTo(0.4, 1);
        this.ground.body.immovable = true;
        
        this.water = this.game.add.sprite(1310, 523, 'water');
        this.game.physics.arcade.enable(this.water);
        this.water.scale.setTo(2, 1);
        //creates animation for water
        this.water.animations.add('movingWater', [0, 1], 2, true);
        
        this.ledge = this.platforms.create(400, 400, 'bricks');
        this.ledge.body.immovable = true;
        
        this.ledge = this.platforms.create(-150, 250, 'bricks');
        this.ledge.body.immovable = true;
        

        //adds diamond as a reward
        this.diamond = this.game.add.sprite(950, 0, 'diamond');
        this.game.physics.arcade.enable(this.diamond);
        this.diamond.body.gravity.y = 300;
        
        //creates star group
        this.stars = this.add.group();
        this.stars.enableBody = true;
        
        //adds stars to game as a reward
        for(var i = 0; i < 20; i++){
            if(i == 10 || i == 11 || i == 17 || i == 9 || i == 14 || i == 15 || i == 19){}
            else{
            var star = this.stars.create(i * 100, 0, 'star');
            
            star.body.gravity.y = 300;
            
            star.body.bounce.y = 0.7;
            }
        }
        
        //adds first enemy
        //runs on ground following player
        this.enemy1 = this.game.add.sprite(600, this.game.world.height - 96, 'baddie');
        this.game.physics.arcade.enable(this.enemy1);
        this.enemy1.body.gravity.y = 300;
        
        //adds second enemy
        //protects the final ledge
        this.enemy2 = this.game.add.sprite(1700, 0, 'baddie');
        this.game.physics.arcade.enable(this.enemy2);
        this.enemy2.body.gravity.y = 300;
        this.enemy2.body.velocity.x = -100;
        
        //adds player
        this.player = this.game.add.sprite(32, this.game.world.height - 150, 'dude');
        this.game.physics.arcade.enable(this.player);
        
        //adds final enemy (bird)
        //follows the player horizontally/vertically
        //able to get "stuck" to platforms (on purpose to make level more manageable)
        this.bird = this.game.add.sprite(1000, 0, 'bird');
        this.game.physics.arcade.enable(this.bird);
        
        //creates animations for the bird
        this.bird.animations.add('flyleft', [3, 4, 5, 4], 10, true);
        this.bird.animations.add('flyright', [0, 1, 2, 1], 10, true);
        
        //sets player physics
        this.player.body.bounce.y = 0.2;
        this.player.body.gravity.y = 300;
        this.player.body.collideWorldBounds = true; //collides with the edge of the world
        
        //create player animations for walking
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right', [5, 6, 7, 8], 10, true);
        
        //create first enemy animations for walking
        this.enemy1.animations.add('enleft', [0, 1], 10, true);
        this.enemy1.animations.add('enright', [2, 3], 10, true);
        
        //creates second enemy animations for walking (same as above)
        this.enemy2.animations.add('enleft', [0, 1], 10, true);
        this.enemy2.animations.add('enright', [2, 3], 10, true);
        
        //allows user to move player using the keyboard <left/right/up/down> keys
        this.buttonFun = this.game.input.keyboard.createCursorKeys();
        
        //follows the player and moves the camera when necessary (player stays centered when not in first/last frame)
        this.camera.follow(this.player);
    },
    update: function(){
        
        //sets collisions among the objects
        this.game.physics.arcade.collide(this.player, this.platforms);
        this.game.physics.arcade.collide(this.trophy, this.platforms);
        this.game.physics.arcade.collide(this.diamond, this.platforms);
        this.game.physics.arcade.collide(this.bird, this.platforms);
        this.game.physics.arcade.collide(this.enemy1, this.platforms);
        this.game.physics.arcade.collide(this.enemy2, this.platforms);
        
        //makes it so the first enemy and bird follow the player
        this.game.physics.arcade.moveToXY(this.enemy1, this.player.body.x, this.game.world.height - 96, 50);
        this.game.physics.arcade.moveToXY(this.bird, this.player.body.x, this.player.body.y, 125);
        
        //starts water animation right away
        this.water.animations.play('movingWater');
        
        
        //sets movement for the enemy guarding the last ledge
        if(this.enemy2.body.x < 1625){
            this.enemy2.body.velocity.x = 150;
        }
        if(this.enemy2.body.x > 1800){
            this.enemy2.body.velocity.x = -150;
        }
        
        
        //starts animations for bird flight
        if(this.bird.body.velocity.x < 0){
            this.bird.animations.play('flyleft');
        }
        else if(this.bird.body.velocity.x > 0){
            this.bird.animations.play('flyright');
        }
        else{
            this.bird.animations.stop();
            
        }
        
        //starts animations for first enemy walking
        if(this.enemy1.body.velocity.x < 0){
            this.enemy1.animations.play('enleft');
        }
        else if(this.enemy1.body.velocity.x > 0){
            this.enemy1.animations.play('enright');
        }
        else{
            this.enemy1.animations.stop();
        }
        
        //starts animations for second enemy walking
        if(this.enemy2.body.velocity.x < 0){
            this.enemy2.animations.play('enleft');
        }
        else if(this.enemy2.body.velocity.x > 0){
            this.enemy2.animations.play('enright');
        }
        else{
            this.enemy2.animations.stop();
        }
        
        //overlap between first enemy and water (kills enemy)
        this.game.physics.arcade.overlap(this.enemy1, this.water, function(enemy, water){
            enemy.kill();
        }, null, this.game);
        
        //overlap between second enemy and water (kills enemy)
        //note: should not happen in current implementation, but still here for potential further use
        this.game.physics.arcade.overlap(this.enemy2, this.water, function(enemy, water){
            enemy.kill();
        }, null, this.game);
        
        //overlap between player and star rewards
        //removes the star and adds to player score
        this.game.physics.arcade.overlap(this.player, this.stars, function(player, star){
            star.kill();
            Fiala.score += 10;
        }, null, this.game);

        //overlap between player and diamond reward
        //removes the diamond and adds to player score
        this.game.physics.arcade.overlap(this.player, this.diamond, function(player, diamond){
            diamond.kill();
            Fiala.score += 50;
        }, null, this.game);
        
        //overlap between player and first enemy
        //kills both player and enemy
        //displays the player score with "GAME OVER" screen
        //after brief pause goes back to MainMenu state
        this.game.physics.arcade.overlap(this.player, this.enemy1, function(player, enemy){
            player.kill();
            enemy.kill();
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //overlap between player and second enemy
        //kills both player and enemy
        //displays the player score with "GAME OVER" screen
        //after brief pause goes back to MainMenu state
        this.game.physics.arcade.overlap(this.player, this.enemy2, function(player, enemy){
            player.kill();
            enemy.kill();
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //overlap between player and bird
        //kills both player and enemy
        //displays the player score with "GAME OVER" screen
        //after brief pause goes back to MainMenu state
        this.game.physics.arcade.overlap(this.player, this.bird, function(player, bird){
            player.kill();
            bird.kill();
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(bird){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //overlap between player and water
        //kills player, but not water
        //displays the player score with "GAME OVER" screen
        //after brief pause goes back to MainMenu state
        this.game.physics.arcade.overlap(this.player, this.water, function(player, water){
            player.kill();
            var txt = this.add.text((this.camera.width / 2), this.camera.height / 2, 'GAME OVER \n Score: ' + Fiala.score);
            txt.anchor.setTo(0.5, 0.5);
            txt.fixedToCamera = true;
            Fiala.score = 0;
            this.time.events.add(4000, function(enemy){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //overlap between player and finish line/trophy
        //removes the trophy
        //after brief pause goes back to MainMenu state, which then goes to the second level on pressing "start"
        this.game.physics.arcade.overlap(this.player, this.trophy, function(player, trophy){
            trophy.kill();
            Fiala.picklevel = 1; //chooses next level
            this.time.events.add(500, function(){
                this.state.start("MainMenu");
            }, this);
        }, null, this.game);
        
        //makes it so the stars collide with the platforms/ground
        this.game.physics.arcade.collide(this.stars, this.platforms);

        //sets the player velocity on x to 0 so that the player stops moving
        this.player.body.velocity.x = 0;
        
        //moves the player according to the keyboard arrow buttons being pressed
        if(this.buttonFun.left.isDown){
            this.player.body.velocity.x = -150;
            this.player.animations.play('left');
        }
        else if(this.buttonFun.right.isDown){
            this.player.body.velocity.x = 150;
            this.player.animations.play('right');
        }
        else{
            this.player.animations.stop();
            this.player.frame = 4;
        }
        if(this.buttonFun.up.isDown && this.player.body.touching.down){
            this.player.body.velocity.y = -350;
        }
    },
    render: function(){
        //kept to display player score
        this.game.debug.text('Score: ' + Fiala.score, 32, 32);
    }
};